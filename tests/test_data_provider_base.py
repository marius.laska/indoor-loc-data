import unittest
import numpy as np
from data.data_provider_base import DataProviderBase


class TestDataProviderBase(unittest.TestCase):

    def test_data_generation(self):
        num_acc = 25
        N = 1000
        rss_min = -99
        rss_max = -38
        rss = np.random.rand(N, num_acc) * (rss_max - rss_min) + rss_min

        # 20% missing values
        mask = np.where(np.random.rand(N, num_acc) > 0.8)
        rss[mask] = 0

    def test_access_point_filter(self):

        num_data = 100
        num_acc = 50

        data = np.array(np.random.rand(num_data, num_acc))
        times = np.array(np.random.rand(num_data))

        # 20% missing values
        mask = np.where(np.random.rand(num_data, num_acc) > 0.8)
        data[mask] = 0

        # mappings
        mappings = np.random.choice(num_data, num_data, replace=False)
        a_len = 20
        mappings = [mappings[i*a_len:(i+1)*a_len] for i in range(int(len(mappings)/a_len))]

        dp = DataProviderBase()
        dp.data_tensor = data
        dp.timestamps = times
        dp.filter_temporary_access_points(mappings, temp_threshold=0.7)

