import numpy as np


def sample_from_beta(min, max, prec=7, size=None):

    c_norm = (0 - min) / (max - min)

    dist = np.random.beta(c_norm * prec, prec * (1 - c_norm), size)

    # scale back to original range
    val = dist * (max - min) + min

    return val


if __name__ == "__main__":
    ax = sns.distplot(sample_from_beta(-5, 20, 20, 10000), hist=True)
    ax.set(xlim=(-5, 20))
    plt.show()
