import logging

import pandas as pd
import numpy as np
import random
from data.data_provider_base import DataProviderBase
from pkg_resources import resource_filename
from sklearn import preprocessing
from datetime import datetime

from data.filters import DeviceFractionFilter

log = logging.getLogger(__name__)
TIME_FORMAT = '%Y-%m-%d %H:%M:%S'
pkg_name = "data"


class LohanDSprovider(DataProviderBase):

    def __init__(self, random_state=1, split_ratio=None, num_epochs=1):
        super().__init__()

        self.floorplan_width = 200
        self.floorplan_height = 80

        self.rss_df_train = None
        self.locations_df_train = None
        self.device_df_train = None
        self.date_df_train = None

        self.rss_df_test = None
        self.locations_df_test = None
        self.device_df_test = None
        self.date_df_test = None

        self.device_enc = None

        self.delete_indices = None
        self.num_epochs = num_epochs

        if split_ratio is None:
            self.split_ratio = {'val': 0.1, 'test': 0.2}
        else:
            self.split_ratio = split_ratio

        self.random_state = random_state

    @classmethod
    def setup_data_provider(cls, data_params, pre_params):
        split_ratio = None
        if 'split_ratio' in data_params:
            split_tuple = data_params['split_ratio']
            split_ratio = {'train': split_tuple[0],
                           'val': split_tuple[1],
                           'test': split_tuple[2]}

        if 'num_epochs' in pre_params:
            num_epochs = pre_params['num_epochs']
            if 'epoch' in pre_params:
                epoch = pre_params['epoch']
            else:
                epoch = 1
        else:
            num_epochs = 1
            epoch = 1

        if 'floor' not in pre_params:
            log.error("'floor' parameter must be supplied. Set to 0...")
            floor = 0
        else:
            floor = pre_params['floor']

        dp = LohanDSprovider(random_state=random.seed(),
                             split_ratio=split_ratio,
                             num_epochs=num_epochs)

        dp.load_dataset(floor=floor, epoch=epoch)
        dp.replace_missing_values()

        if 'standardize' not in pre_params or pre_params['standardize']:
            dp.standardize_data()

        # set up all filters that are sequentially applied to generate
        # the desired subset of data
        data_filters = dp.setup_filters(pre_params)
        if len(data_filters) > 0:
            dp.convert_to_dataframe()
            dp.apply_filters(data_filters)
            dp.transform_into_tensor(missing_val=0)

        # remove temporary APs
        if "filter_tmp_AP" in pre_params and pre_params[
                "filter_tmp_AP"] is not None:
            dp.filter_temporary_access_points(
                temp_threshold=pre_params["filter_tmp_AP"])

        if dp.splits_indices is None:
            dp.generate_split_indices(pre_params)

        return dp

    def load_dataset(self, floor=0, epoch=1):
        # read training dataframes
        self.locations_df_train = pd.read_csv(resource_filename(pkg_name, "lohan/Training_coordinates_21Aug17.csv"), delimiter=',', header=None)
        self.rss_df_train = pd.read_csv(resource_filename(pkg_name, "lohan/Training_rss_21Aug17.csv"), delimiter=',', header=None)
        self.device_df_train = pd.read_csv(resource_filename(pkg_name, "lohan/Training_device_21Aug17.csv"), delimiter=',', header=None)
        self.date_df_train = pd.read_csv(resource_filename(pkg_name, "lohan/Training_date_21Aug17.csv"), delimiter=',', header=None)

        # read test dataframes
        self.locations_df_test = pd.read_csv(resource_filename(pkg_name, "lohan/Test_coordinates_21Aug17.csv"), delimiter=',', header=None)
        self.rss_df_test = pd.read_csv(resource_filename(pkg_name, "lohan/Test_rss_21Aug17.csv"),
                                        delimiter=',', header=None)
        self.device_df_test = pd.read_csv(resource_filename(pkg_name,
            "lohan/Test_device_21Aug17.csv"), delimiter=',', header=None)
        self.date_df_test = pd.read_csv(resource_filename(pkg_name,
            "lohan/Test_date_21Aug17.csv"), delimiter=',', header=None)

        # reduce to correct floor
        floor_ids_train = np.where(self.locations_df_train.values[:, 2] == floor)[0]
        floor_ids_test = np.where(self.locations_df_test.values[:, 2] == floor)[0]

        # reduce to time interval
        num_data_train = len(floor_ids_train)
        num_data_test = len(floor_ids_test)
        num_epochs = self.num_epochs
        epoch_len_train = int(num_data_train / num_epochs) * epoch
        epoch_len_test = int(num_data_test / num_epochs) * epoch

        # sort
        epoch_ids_train = self.date_df_train.iloc[floor_ids_train, :].sort_values(by=0).index.values
        epoch_ids_test = self.date_df_test.iloc[floor_ids_test, :].sort_values(by=0).index.values

        sub_idx_train = epoch_ids_train[:epoch_len_train]
        sub_idx_test = epoch_ids_test[:epoch_len_test]

        train_data = self.rss_df_train.values[sub_idx_train, :]
        test_data = self.rss_df_test.values[sub_idx_test, :]

        # date of recording
        train_dates = self.date_df_train.values[sub_idx_train, :]
        test_dates = self.date_df_test.values[sub_idx_test, :]

        # device label encoding
        train_uids = self.device_df_train.values[sub_idx_train]
        test_uids = self.device_df_test.values[sub_idx_test]

        uids = np.concatenate((train_uids, test_uids))
        device_enc = preprocessing.LabelEncoder()
        self.uids = device_enc.fit_transform(np.ravel(uids))
        self.device_enc = device_enc

        train_labels = self.locations_df_train.values[sub_idx_train][:, [0, 1]]
        test_labels = self.locations_df_test.values[sub_idx_test][:, [0, 1]]

        self.date_tensor = np.ravel(np.concatenate((train_dates, test_dates)))
        self.data_tensor = np.concatenate((train_data, test_data), axis=0)
        self.labels = np.concatenate((train_labels, test_labels), axis=0)

    def get_data_for_user(self, uid):
        mask = np.where(self.uids == uid)[0]
        data = self.data_tensor[mask, :]
        labels = self.labels[mask, :]

        return data, labels

    def get_num_user(self):
        if self.uids is None:
            return 0
        return len(np.unique(self.uids))

    def replace_missing_values(self, mis_val=100, replace_val=-110):
        self.data_tensor[self.data_tensor == mis_val] = replace_val

    def standardize_values(self):
        min_AP_val = np.min(self.data_tensor)
        max_AP_val = np.max(self.data_tensor)
        self.data_tensor = (self.data_tensor - min_AP_val) / (
                    max_AP_val - min_AP_val)

    def convert_to_dataframe(self, missing_value=0):

        frames = []

        for mp_idx, (rss_row, date, label, uid) in enumerate(zip(self.data_tensor, self.date_tensor, self.labels, self.uids)):
            # non 0 values
            non_zero_mask = np.where(rss_row != missing_value)[0]

            # convert data to unix timestamp
            date = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
            timestamp = date.timestamp()

            rss_sub = rss_row[non_zero_mask]
            df_dict = {'id': [mp_idx] * len(non_zero_mask),
                       'createdBy': [uid] * len(non_zero_mask),
                       'createdAt': [timestamp] * len(non_zero_mask),
                       'timeStamp': [timestamp] * len(non_zero_mask),
                       'rssValue': rss_sub.tolist(),
                       'macAddress': non_zero_mask.tolist(),
                       'cellX': label[0],
                       'cellY': label[1],
                       'mapId': 1}

            df = pd.DataFrame(df_dict)

            frames.append(df)

        frame = pd.concat(frames)

        self.measurements_df = frame
        self.mac_addresses_df = pd.DataFrame(np.arange(self.data_tensor.shape[1]), columns=["macAddress"])


if __name__ == "__main__":

    dp = LohanDSprovider(random_state=random.seed())
    dp.load_dataset(floor=0, epoch=5)
    dp.replace_missing_values(mis_val=100, replace_val=-110)
    #dp.standardize_values()
    dp.convert_to_dataframe(missing_value=-110)

    # setup device filter
    device_filter = DeviceFractionFilter(
        "createdBy", 15, 0.8, method="filter")

    dp.apply_filters([device_filter])

    dp.transform_into_tensor()

    print("test")





