import numpy as np
import pandas as pd
from scipy import std
from scipy.stats import zscore
from sqlalchemy import create_engine

import logging

from data.data_provider_base import DataProviderBase
from data.filters import IsInFilter, DeviceFractionFilter, \
    LocationFractionFilter

from data.utility.math_utils import sample_from_beta

log = logging.getLogger(__name__)


class PDdataProvider(DataProviderBase):
    """
    Class that generates and transforms a Indoor localization dataset
    via SQL connection to the Backend database.

    -> allows for filtering (via columnValue, userID and location) for
    artificially manipulating the dataset for hypothesis testing
    """

    def __init__(self, conn_str=None, map_id=None, split_ratio=None):
        super().__init__()

        self.__conn_str = conn_str
        self.map_id = map_id

        self.total_area = 0
        self.origin = "ul_inv"
        self.label_dim = "grid"

        self.mac_addresses_df = None
        self.measurements_df: pd.DataFrame = None
        self.col_df: pd.DataFrame = None
        self.filtered_df: pd.DataFrame = None

        self.uids = None

        self.split_ratio = split_ratio

    @classmethod
    def setup_data_provider(self, data_params, pre_params=None):
        """
        Factory method for data provider that uses filters to obtain subset
        of data.
        1) The labels are transformed from grid dimension with origin at upper left
        (android drawing) to lower left with meter dimension.
        2) Filters are applied to obtain base set of data
        3) Data is transformed into tensor and standardized (0-std columns (APs)
             are discarded)
        4) Data is split into several folds, such that the specific data
            (train/val/test) can be accessed for the current split index of the
            class
        :param data_params: Data parameters dict that holds connection and map_id
        :param filters: The filters parsed from yaml (via ConfigReader)
        :return:
        """
        connection_str = data_params['connection']
        map_id = data_params['map_id']
        split_ratio = None
        if 'split_ratio' in data_params:
            split_tuple = data_params['split_ratio']
            split_ratio = {'train': split_tuple[0],
                           'val': split_tuple[1],
                           'test': split_tuple[2]}

        dataset_provider = PDdataProvider(connection_str, map_id, split_ratio)

        # read SQL dataset from Backend

        if type(map_id) is list:
            dataset_provider.get_building_dataset()
            dataset_provider.test_transform_into_tensor()
        else:
            dataset_provider.get_dataset()

            # convert to meters
            dataset_provider.transform_labels_from_grid_to_meter()
            dataset_provider.transform_labels_origin(origin="ll")

            # set up all filters that are sequentially applied to generate
            # the desired subset of data
            data_filters = dataset_provider.setup_filters(pre_params=pre_params)
            dataset_provider.apply_filters(data_filters)

            dataset_provider.transform_into_tensor()

        # add label noise
        if 'label_noise' in pre_params:
            param = pre_params['label_noise']
            if type(param) is list:
                dataset_provider.add_label_noise(*pre_params['label_noise'])
            else:
                dataset_provider.add_noise_within_walls(walls_file=param)

        # if we replace missing values by area mean, we cannot standardize yet
        if 'missing_values' not in pre_params and (
                'standardize' not in pre_params or pre_params['standardize']):
            dataset_provider.standardize_data()

        # remove temporary APs
        if "filter_tmp_AP" in pre_params and pre_params[
            "filter_tmp_AP"] is not None:
            dataset_provider.filter_temporary_access_points(
                temp_threshold=pre_params["filter_tmp_AP"])

        if dataset_provider.splits_indices is None:
            dataset_provider.generate_split_indices(pre_params)

        return dataset_provider

    def get_building_dataset(self):
        """
        Generates a dataset containing multiple floors => for floor prediction
        :return:
        """

        m_df = []
        mac_df = []

        # get maps
        for map_id in self.map_id:
            mac_addresses_df, measurement_df = self.get_dataset(map_id)
            m_df.append(measurement_df)
            mac_df.append(mac_addresses_df)

        self.measurements_df = m_df
        self.mac_addresses_df = mac_df

    def get_dataset(self, map_id=None):
        """
        Reads the dataset from the SQL database and sets up the initial
        pandas dataframe structures
        :return:
        """
        if map_id is None:
            map_id = self.map_id

        # initiate database connection
        engine = create_engine(self.__conn_str)

        # get map dimensions to calc total area
        dim_query = """SELECT "widthInMeter", "heightInMeter" FROM mapmetadata WHERE id={}"""
        dim_pd = pd.read_sql_query(dim_query.format(map_id), con=engine)
        self.floorplan_height = float(dim_pd['heightInMeter'])
        self.floorplan_width = float(dim_pd['widthInMeter'])
        self.total_area = float(self.floorplan_width * self.floorplan_height)

        # query for measurements dataframe
        c_query_str = """
                    SELECT mp.id, "createdBy", mp."createdAt", "timeStamp", "rssValue", m."macAddress", "cellX", 
                    "cellY", "mapId" FROM measurementprocessresult as mp JOIN measurements ON 
                    mp.id=measurements."processResult" 
                    JOIN macaddress AS m on m.id = measurements."macAddress"
                    {} ORDER BY id
                    """

        where_clause = self.get_where_clause_for_measurements(map_id=map_id,
                                                              autoAnnotated=None)
        c_query_str = c_query_str.format(where_clause)
        measurements_df = pd.read_sql_query(c_query_str, con=engine)

        self.mac_addresses_df = measurements_df.drop_duplicates(
            subset="macAddress").loc[:, ["macAddress"]].sort_values(
            "macAddress")
        self.measurements_df = measurements_df

        return self.mac_addresses_df, self.measurements_df

    def standardize_data(self, apply_z_score=False, column_wise=True):
        """
        Standardizes the dataset (removes columns with std=0) by applying
        z-score normalization.
        :return:
        """
        if self.data_tensor is None:
            return

        if not apply_z_score:
            min_AP_val = np.min(self.data_tensor)
            max_AP_val = np.max(self.data_tensor)
            self.data_tensor = (self.data_tensor - min_AP_val) / (
                        max_AP_val - min_AP_val)
            return

        col_std = self.col_df.apply(std)
        non_zero_std = np.where(col_std != 0)[0]

        self.col_df = self.col_df.iloc[:, non_zero_std]
        self.mac_addresses_df = self.mac_addresses_df.iloc[non_zero_std]

        self.col_df = self.col_df.apply(zscore)

        self.data_tensor = self.col_df.values

    def get_where_clause_for_measurements(self, map_id, user_id=None,
                                          x_cell_range=None,
                                          y_cell_range=None, time_range=None,
                                          autoAnnotated=None):
        where_clause = "WHERE "

        if map_id is not None:
            where_clause += '"mapId"={} AND '.format(map_id)

        if user_id is not None:
            where_clause += '"createdBy"={} AND '.format(user_id)
        if x_cell_range is not None:
            where_clause += '"cellX" <= {} AND "cellX" >= {} AND '.format(
                x_cell_range[1], x_cell_range[0])

        if y_cell_range is not None:
            where_clause += '"cellY" <= {} AND "cellY" >= {} AND '.format(
                y_cell_range[1], y_cell_range[0])
        if time_range is not None:
            where_clause += 'measurementprocessresult."createdAt" <= {} AND ' \
                            'measurementprocessresult."createdAt" >= {} AND '. \
                format(time_range[1], time_range[0])
        if autoAnnotated is not None:
            where_clause += 'mp."autoAnnotated" = '
            if autoAnnotated:
                where_clause += 'TRUE'
            else:
                where_clause += 'FALSE'

        if where_clause.endswith("AND "):
            where_clause = where_clause[:-4]
        if where_clause.endswith("WHERE "):
            where_clause = where_clause[:-6]

        return where_clause

    def replace_missing_values_and_standardize(self, pre_params):
        if 'missing_values' in pre_params:
            if pre_params['missing_values'] == "area_mean":
                self.replace_missing_values_with_mean()
        if 'standardize' in pre_params and pre_params['standardize']:
            self.standardize_data()

    def replace_missing_values_with_mean(self):
        """
        Replaces missing AP values within each area with the average RSS
        of the specific AP in the given area. Requires the area labels
        of the dataset to be already set.
        :return:
        """
        x_train, y_train = self.get_train_data()
        x_val, y_val = self.get_val_data()

        area_labels = np.concatenate((y_train, y_val), axis=0)
        data = np.concatenate((x_train, x_val), axis=0)

        if area_labels is None:
            log.error("Area labels have not been assigned yet")
            return

        for area_idx in range(area_labels.shape[1]):
            # obtain the subset mask for measurements of the current area
            mask = np.where(area_labels[:, area_idx] == 1)[0]
            if len(mask) == 0:
                continue
            m_data = data[mask, :]
            mean = np.mean(m_data, axis=0)

            # replace missing AP value of subset data with average value
            for col_idx in range(data.shape[1]):
                missing = np.where(m_data[:, col_idx] == -110.0)
                m_data[missing[0], col_idx] = mean[col_idx]

            self.data_tensor[mask, :] = m_data

    def test_transform_into_tensor(self, dfs=None):
        if dfs is None:
            dfs = self.measurements_df

        global_mac_df = None

        global_data = []
        global_labels = []
        global_uids = []
        global_timestamps = []

        for map_idx, (df, mac_df) in enumerate(zip(dfs, self.mac_addresses_df)):

            if global_mac_df is None:
                global_mac_df = mac_df
            else:
                # merge
                global_mac_df = pd.concat([global_mac_df, mac_df])
                global_mac_df = global_mac_df.drop_duplicates()

            pids = df.drop_duplicates(subset=["id"])["id"].values
            num_mac = len(global_mac_df.values)

            tensor = np.full((len(pids), num_mac), -110.0, dtype=float)
            uids = np.zeros(len(pids))
            timestamps = np.zeros(len(pids))
            labels = np.zeros((len(pids), len(self.mac_addresses_df)))
            labels[:, map_idx] = np.ones(len(pids))

            for _, row in df.iterrows():
                mac_idx = np.where(
                    global_mac_df["macAddress"].values == row[
                        "macAddress"])[0]

                pid_idx = np.where(pids == row["id"])[0]
                tensor[pid_idx, mac_idx] = row["rssValue"]
                uids[pid_idx] = row["createdBy"]
                timestamps[pid_idx] = row["timeStamp"]

            global_data.append(tensor)
            global_labels.append(labels)
            global_timestamps.append(timestamps)
            global_uids.append(uids)

        # pad data tensors with missing value for non recorded APs
        max_AP_dim = global_data[-1].shape[1]
        for idx in range(len(global_data) - 1):
            AP_dim = global_data[idx].shape[1]
            num_data = len(global_data[idx])
            global_data[idx] = np.concatenate([global_data[idx], np.full(
                (num_data, max_AP_dim - AP_dim), -110.0)], axis=1)

        self.mac_addresses_df = global_mac_df
        self.labels = np.concatenate(global_labels, axis=0)
        self.data_tensor = np.concatenate(global_data, axis=0)
        self.uids = np.concatenate(global_uids, axis=0)
        self.timestamps = np.concatenate(global_timestamps, axis=0)

        return self.data_tensor, self.labels

    def transform_into_tensor(self, df=None):
        """
        Transforms the pandas dataframe into a tensor (matrix) where each column
        represents a specific access point. If the dataset has a specific column
        'split', with values (train or test) indicating the split for training,
        this is used to generate a single train/test split. Also calculates class
        variable self.uids which holds for each data row the associated user id.
        :return:
        """
        if df is None:
            df = self.filtered_df
        if 'split' in df:
            # generate split indices from df entries
            train_idx = set()
            test_idx = set()

        pids = df.drop_duplicates(subset=["id"])["id"].values
        num_mac = len(self.mac_addresses_df.values)

        tensor = np.full((len(pids), num_mac), -110.0, dtype=float)
        uids = np.zeros(len(pids))
        timestamps = np.zeros(len(pids))
        labels = np.zeros((len(pids), 2))

        for _, row in df.iterrows():
            mac_idx = np.where(
                self.mac_addresses_df["macAddress"].values == row[
                    "macAddress"])[0]

            pid_idx = np.where(pids == row["id"])[0]
            tensor[pid_idx, mac_idx] = row["rssValue"]
            labels[pid_idx, 0] = row["cellX"]
            labels[pid_idx, 1] = row["cellY"]
            uids[pid_idx] = row["createdBy"]
            timestamps[pid_idx] = row["timeStamp"]

            # add row to train or test split if specified
            if 'split' in df:
                if row['split'] == 'train':
                    train_idx.add(pid_idx[0])
                elif row['split'] == 'test':
                    test_idx.add(pid_idx[0])

        self.col_df = pd.DataFrame(data=tensor)

        # setup single fold split indices
        if 'split' in df:
            self.splits_indices = [(np.array(list(train_idx)),
                                    np.array(list(test_idx)))]
            self.num_splits = 1

        self.labels = labels
        self.data_tensor = tensor
        self.uids = uids
        self.timestamps = timestamps

        return tensor, labels

    def apply_filters(self, filters):
        """
        Applies a set of filters on the read dataframe
        :param filters: A list of Filter
        :return:
        """
        self.filtered_df = self.measurements_df
        for filter in filters:
            self.filtered_df = filter.apply_on_df(self.filtered_df)

    #
    # change label dimensions
    #

    def transform_labels_from_grid_to_meter(self, labels=None, grid_size=2,
                                            inverse=False):

        if labels is None:
            labels = self.labels

        if inverse:
            if labels is not None:
                labels = (labels + (0.5 * grid_size)) / grid_size
            if self.measurements_df is not None:
                new = (self.measurements_df[['cellX', 'cellY']] + (
                        0.5 * grid_size)) / grid_size
                self.measurements_df[['cellX', 'cellY']] = new

        else:
            if labels is not None:
                labels = labels * grid_size - (0.5 * grid_size)

            if self.measurements_df is not None:
                new = self.measurements_df[['cellX', 'cellY']] * grid_size - (
                        0.5 * grid_size)
                self.measurements_df[['cellX', 'cellY']] = new
        self.labels = labels
        return labels

    def transform_labels_origin(self, labels=None, origin="ll"):

        # lower left
        if origin == "ll" and self.origin == "ul_inv":

            if self.measurements_df is not None:
                pd_x = self.measurements_df['cellX'].copy()
                pd_y = self.measurements_df['cellY'].copy()

                self.measurements_df['cellX'] = pd_y
                self.measurements_df['cellY'] = self.floorplan_height - pd_x

            if labels is None:
                labels = self.labels
            if labels is not None:
                new_labels = np.zeros(labels.shape)
                new_labels[:, 0] = labels[:, 1]
                new_labels[:, 1] = self.floorplan_height - labels[:, 0]

                self.labels = new_labels

                return new_labels

        else:
            print("wrong conversion of labels")

    def preprocess_test_data_window(self, window_size=3):
        """
        Artificially changes the values of the test data by using the last
        'window_size' elements to calculate values for missing APs. Missing AP
        values are replaced by the average RSS value of the last seen
        measurements.
        """
        x_test, y_test = self.get_test_data()
        for x_idx, x in enumerate(x_test):
            # obtain the subset of data which is used to calculate the AP mean
            lower = max(0, x_idx + 1 - window_size)
            upper = x_idx + 1
            window = x_test[lower:upper]

            mean = np.mean(window, axis=0)

            # update missing AP values of current element by calculated
            # window mean
            missing = np.where(x == -110.0)[0]
            x[missing] = mean[missing]
            x_test[x_idx, :] = x

    def recompute_split_indices_for_fixed_split(self, old_data_len, mask):
        """
        If the train/test split is given by any filter, we have to keep
        the split indices but adapt them to the filtered rows of the data
        :param old_data_len: The length of the non filtered data set
        :param mask: The mask, which is used to obtain subset of data
        :return:
        """
        # generate mapping from old ids to new ids
        # required, since data is shifted by masking
        mapping = np.full(old_data_len, -1)
        last_set = -1
        last_m = 0
        for m in mask:
            mapping[last_m + 1:m] = -1
            mapping[m] = last_set + 1
            last_set += 1
            last_m = m

        train_idx, test_idx = self.splits_indices[0]

        # get subset of train/test indices of computed split
        masked_train_idx = train_idx[np.isin(train_idx, mask)]
        masked_test_idx = test_idx[np.isin(test_idx, mask)]

        # shift the subset of indices by calculated mapping
        shifted_train_idx = np.array([mapping[idx] for idx in masked_train_idx])
        shifted_test_idx = np.array([mapping[idx] for idx in masked_test_idx])

        self.splits_indices[0] = (shifted_train_idx, shifted_test_idx)

    def set_area_labels(self, area_labels, delete_uncovered=True,
                        pre_params=None, noisy=False):
        super().set_area_labels(area_labels, delete_uncovered, pre_params,
                                noisy)
        if delete_uncovered:
            # need to recompute k-fold splitting indices since base data changed
            if hasattr(self, 'filtered_df') and 'split' in self.filtered_df:
                mask = np.where(np.any(area_labels != 0, axis=1))[0]
                self.recompute_split_indices_for_fixed_split(
                    len(area_labels), mask)
            else:
                self.generate_split_indices(pre_params)

    def remove_APs_with_low_correlation_to_areas(self, params):

        subset = super().remove_APs_with_low_correlation_to_areas(params)
        if subset is not None:
            self.mac_addresses_df = self.mac_addresses_df[:, subset]

    def add_label_noise(self, mean, std):
        # add gaussian noise to labels
        noise = np.random.normal(mean, std, (len(self.labels), 2))
        self.noisy_labels = self.labels + noise

    def add_noise_within_walls(self, walls_file):
        self.noisy_labels = np.zeros(self.labels.shape)

        walls = np.load(walls_file)
        v_walls = walls[np.where(walls[:, 0] == walls[:, 2])[0], :]
        h_walls = walls[np.where(walls[:, 1] == walls[:, 3])[0], :]
        for idx, label in enumerate(self.labels):
            # find closest horizontal walls:
            dist_h = h_walls[:, 1] - label[1]

            # restrict to walls that lie in same vertical range
            minimum = np.min(v_walls[:, [0, 2]], axis=1)
            np.less_equal(minimum, label[0])

            range_test_s = np.less_equal(np.min(h_walls[:, [0, 2]], axis=1),
                                         label[0])
            range_test_l = np.greater_equal(np.max(h_walls[:, [0, 2]], axis=1),
                                            label[0])

            dist_h = dist_h[
                np.where(np.logical_and(range_test_s, range_test_l))[0]]

            pos_d_h = dist_h[np.where(dist_h >= 0)[0]]
            if len(pos_d_h) == 0:
                off_y_pos = 0
            else:
                off_y_pos = np.min(pos_d_h)

            neg_d_h = dist_h[np.where(dist_h < 0)[0]]
            if len(neg_d_h) == 0:
                off_y_neg = 0
            else:
                off_y_neg = np.max(neg_d_h)

            # generate y noise
            y_noise = sample_from_beta(off_y_neg, off_y_pos, prec=20)
            # y_noise = np.random.uniform(off_y_neg, off_y_pos)

            # get distance vector to vertical walls
            dist_v = v_walls[:, 0] - label[0]

            # restrict to walls that lie in same vertical range
            range_test_s = np.less_equal(np.min(v_walls[:, [1, 3]], axis=1),
                                         label[1])
            range_test_l = np.greater_equal(np.max(v_walls[:, [1, 3]], axis=1),
                                            label[1])

            dist_v = dist_v[
                np.where(np.logical_and(range_test_s, range_test_l))[0]]

            pos_d_v = dist_v[np.where(dist_v >= 0)[0]]
            if len(pos_d_v) == 0:
                off_x_pos = 0
            else:
                off_x_pos = np.min(pos_d_v)

            neg_d_v = dist_v[np.where(dist_v < 0)[0]]
            if len(neg_d_v) == 0:
                off_x_neg = 0
            else:
                off_x_neg = np.max(neg_d_v)

            # generate x noise
            x_noise = sample_from_beta(off_x_neg, off_x_pos, prec=20)
            # x_noise = np.random.uniform(off_x_neg, off_x_pos)

            self.noisy_labels[idx, :] = np.array(
                [label[0] + x_noise, label[1] + y_noise])

    def get_correct_class_label_percentage_after_noise(self):
        n_l = np.argmax(self.noisy_area_labels, axis=1)
        l = np.argmax(self.area_labels, axis=1)


if __name__ == "__main__":
    data_provider = PDdataProvider(conn_str="", map_id=1)

    # read SQL dataset from Backend
    data_provider.get_dataset()

    # setup simple column filter
    mac_addresses = data_provider.mac_addresses_df["macAddress"].values
    mac_filter = IsInFilter("macAddress", mac_addresses[:5])

    # setup device filter
    device_filter = DeviceFractionFilter(
        "createdBy", 15, 0.8, method="keep")

    # setup two location filters
    location_filter = LocationFractionFilter(
        x_filter=[6, 8], y_filter=[2, 3], percentage=0.5, method="keep")

    location_filter_bespr = LocationFractionFilter(
        x_filter=[5, 9], y_filter=[16, 19], percentage=0.5, method="keep")

    filters = [device_filter, location_filter, location_filter_bespr]

    data_provider.apply_filters(filters)

    data_provider.transform_into_tensor()

    data_provider.standardize_data()

    data_provider.generate_split_indices()

    print("test")

