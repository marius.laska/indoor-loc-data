import random
import pandas as pd


class Filter:
    """
    Class that is used to filter an existing pandas dataframe
    """
    def __init__(self, df: pd.DataFrame=None):
        self.df = df

    def apply(self) -> pd.DataFrame:
        pass

    def apply_on_df(self, df) -> pd.DataFrame:
        self.df = df
        return self.apply()


class IsInFilter(Filter):
    """
    Class to apply filter out values with column values not present in the
    filter list
    """
    def __init__(self, col, filter_list, df=None):
        super().__init__(df)

        self.col = col
        self.filter_list = filter_list

    def apply(self):
        self.df = self.df[self.df[self.col].isin(self.filter_list)]
        return self.df


class RangeFilter(Filter):
    """
    Class to filter out values with specific column value in range
    """
    def __init__(self, col, range, mode=None, df=None):
        super().__init__(df)

        self.col = col
        self.range = range
        self.mode = mode

    def apply(self):
        if self.mode == "train_test_filter":
            if 'split' not in self.df:
                self.df['split'] = 'train'
            self.df.loc[self.df[self.col].between(*self.range), 'split'] = 'test'
        else:
            self.df = self.df[self.df[self.col].between(*self.range)]
        return self.df


class DeviceFractionFilter(Filter):
    """
    Filter class to select a subset of devices.
    Used to select only a subset of measurements of a specific userID.
    For example: Only take 15% of the measurements of user X.
    If split_column is true, the dataset is not filtered but a separate column
    ('filter') is added, which holds values of (train/val/test)
    => used for specific data set split based on device
    """
    def __init__(self, col, value, percentage, method="filter", mode=None, df=None):
        """
        Constructor
        :param col: The column name on which filter is applied
        :param value: The value that column should have
        :param percentage: The percentage of data that is kepts
        :param method: Whether to drop the data that does not match the filtered
        column.
            - "filter" keeps values that do not match the "value".
            - "discard" erases all values that do not match the "value".
        :param df: The pandas dataframe on which filter is applied
        """
        super().__init__(df)

        self.col = col
        self.value = value
        self.percentage = percentage
        self.mode = mode
        self.method = method

    def apply(self):
        # only look at mpID
        only_pid = self.df.drop_duplicates(subset=["id"])

        if self.value is None:
            user_pid = only_pid[pd.isna(only_pid[self.col])].id.values
        else:
            user_pid = only_pid[
                only_pid[self.col] == self.value].id.values

        # either use test set ratio if (train/test) tuple is supplied or
        # set to sampling ratio (for general filter mode)
        ratio = self.percentage[1] if isinstance(self.percentage,
                                                 list) else self.percentage

        user_pid_sample = random.sample(population=set(user_pid),
                                    k=int(ratio * len(user_pid)))

        if self.mode == "train_test_filter":
            # do not alter base data set but add column, which stores info
            # whether data entry belongs to train, val, or test split
            if 'split' not in self.df:
                self.df['split'] = 'train'
            self.df.loc[self.df["id"].isin(user_pid_sample), 'split'] = 'test'
            return self.df

        else:
            sample_df = self.df[self.df["id"].isin(user_pid_sample)]

            rmv_df = self.df[~self.df["id"].isin(user_pid)]

            if self.method == "filter":
                self.df = pd.concat([rmv_df, sample_df])
            elif self.method == "discard":
                self.df = sample_df

            return self.df


class LocationFractionFilter(Filter):
    """
    Class to filter out measurements based on their tagged location.
    For example: Only take 15% of measurements taken in rectange specified by
    bounds (x_filter, y_filter).
    """
    def __init__(self, x_filter, y_filter, percentage, method="filter", df=None):
        super().__init__(df)

        self.x_filter = x_filter
        self.y_filter = y_filter
        self.percentage = percentage
        self.method = method

    def apply(self):

        only_pid = self.df.drop_duplicates(subset=["id"])

        x_filtered = only_pid[only_pid["cellX"].between(self.x_filter[0],
                                                        self.x_filter[1])]

        y_filtered = x_filtered[x_filtered["cellY"].between(self.y_filter[0],
                                                            self.y_filter[1])]

        y_filtered_id = y_filtered["id"].values

        # base data set with location removed
        rmv_df = self.df[~self.df["id"].isin(y_filtered_id)]

        # sample subset
        loc_sample = random.sample(population=set(y_filtered_id),
                                    k=int(self.percentage * len(y_filtered_id)))

        if self.mode == "train_test_filter":
            # do not alter base data set but add column, which stores info
            # whether data entry belongs to train, val, or test split
            if 'split' not in self.df:
                self.df['split'] = 'train'
            self.df.loc[self.df["id"].isin(loc_sample), 'split'] = 'test'
            return self.df

        else:
            sample_df = self.df[self.df["id"].isin(loc_sample)]

            if self.method == "filter":
                self.df = pd.concat([rmv_df, sample_df])
            elif self.method == "discard":
                self.df = sample_df

            return self.df


class MultipleDeviceFractionFilter(Filter):
    def __init__(self, filters, df=None):
        super().__init__(df)

        self.mode = filters['mode']
        self.filters = filters['filters']

    def apply(self):

        for filter in self.filters:
            dev_filter = DeviceFractionFilter('createdBy', filter['user_id'],
                                              filter['ratio'], method='filter')
            self.df = dev_filter.apply_on_df(self.df)

        if self.mode == "discard":
            only_pid = self.df.drop_duplicates(subset=["id"])

            u_ids = [filter['user_id'] for filter in self.filters]

            filtered = only_pid[only_pid['createdBy'].isin(u_ids)]

            filtered_ids = filtered["id"].values

            # base data set with location removed
            self.df = self.df[self.df["id"].isin(filtered_ids)]

        return self.df


class MultipleLocationFractionFilter(Filter):
    def __init__(self, filters, df=None):
        super().__init__(df)

        self.mode = filters['mode']
        self.filters = filters['filters']

    def apply(self):

        for filter in self.filters:
            loc_filter = LocationFractionFilter(
                filter['x'], filter['y'], filter['ratio'], "filter")
            self.df = loc_filter.apply_on_df(self.df)

        if self.mode == "discard":
            only_pid = self.df.drop_duplicates(subset=["id"])

            values = []
            for filter in self.filters:
                filtered = only_pid
                x_filter = filter['x']
                y_filter = filter['y']

                filtered = filtered[filtered['cellX'].between(x_filter[0],
                                                              x_filter[1])]
                filtered = filtered[filtered['cellY'].between(y_filter[0],
                                                              y_filter[1])]
                values += filtered["id"].values.tolist()

            # base data set with location removed
            self.df = self.df[self.df["id"].isin(values)]

        return self.df